# gitdemo
Ett projekt för att introducera git genom att tillåta att man sandboxar

## Vad är Git?
Git är ett Distributed Version Control System (DVCS). Fancy schmancy ord. Väldigt kort och gott förklarat kan man säga att Git är ett sätt att hålla koll på filändringar som sker i en folder (foldern kallas inom git för repository). Den ger då tillåtelse att spara de ändringarna (kallas för en commit) för att ge en tydlig historik av genomförda ändringar. Commit historiken gör det lätt att se vem som genomfört en ändring, när den gjordes, vilka filer som ändrades på och vilka förändringar som skedde och även möjlighet att gå tillbaka och återställa till en tidigare commit, om låt oss säga en bug introduceras.

## Varför använder man Git?
Det finns flera användningsområden för Git, bland annat...
- Att se vilka förändringar som sker, av vem och när (commit history)
- Att lätt och smidigt kunna gå tillbaka till en tidigare version av projektet (revert)
- Att flera personer kunna tillsammans arbeta på samma projekt, att vardera person ska kunna arbeta med filerna på sin dator utan att det stör varandras arbete men att dessa förändringar sedan kan slås ihop (merge)
- Att underlätta samarbete med andra parter som man kanske inte fullständigt litar på än, genom att möjliggöra att de får tillgång till filer och föreslå ändringar - men inte genomföra ändringar (pull request)
- Att kunna ha flera versioner av projektet som existerar parallellt till varandra och snabbt möjliggöra att hoppa mellan de versionerna, exempelvis om man har kod som finns i production, annan kod som testas och ytterliggare en tredje version av koden för det som utvecklas (branches)

## Hur fungerar Git?
Git behöver installeras på din dator och går att installera på [Gits officiella hemsida](https://git-scm.com/). För att Git ska lägga märke till vilka filändringar som sker inom din projektmapp behöver du först berätta för git att den mappen är av intresse. Detta kallas att initiera ett git repository. För att göra det, använd din command line för att navigera till din projektmapp och sedan skriv följande command för att initiera repositoryt.

~~~~
cd C:\Path\ToMyProject
git init
~~~~

När det väl är gjort kommer git att lägga till en dold .git mapp i din projektmapp, där kommer all information om ditt repository så som commit historiken, existerande branches och mycket annat att samlas. Bara för att testa, lägg till en fil i din projektmapp! Det kan vara en text fil eller vad som helst annat. Skriv sedan återigen i din command line...

~~~~
git status
~~~~

Troligtvis kommer du då att se något i still med "Unstaged changes: din-fil". Eftersom att man vill att commit historiken ska vara lätt överskådlig, att man ska veta vilka ändringar som har skett till projektet, kan det vara värdefullt att gruppera ändringar till flera filer in i en och samma commit. Eller tvärtom! Se till att ha två ändringar i samma fil som två separata commits för att de berör helt olika saker. Därför kan ändrade filer befinna sig i 2 states: Unstaged och Staged. Allting som är Staged kommer att läggas till i din commit. För att stagea en ändring behöver du skriva:

~~~~
git add din-fil
~~~~

Detta kommer att stagea din-fil och om du återigen skriver git status kommandot lär det istället stå "Staged changes: din-fil". Om du har gjort ändringar till flera filer kanske du gärna vill lägga till alla dessa på en och samma gång, då skriver du:

~~~~
git add .
~~~~

Punkten betyder i detta sammanhang alla.
Sist men inte minst gäller det nu att lägga till förändringar i din historik och då genomför vi något som man kallar för en commit. Alla commits behöver en commit message, det vill säga ett meddelande där man kan skriva en kort sammanfattning, helst bara några ord eller kortare mening, för att beskriva ändringarna man gjort

~~~~
git commit -m "add file"
~~~~

Det kan vara bra att förstå att git är byggt för command line integration men att det är långt ifrån det enda sättet man kan använda sig utav git. Det finns många bra verktyg för att visualisera Git flödet, där man kan lättare se commit historiken, stagea, unstagea, commita, skapa branches och så mycket mer! Jag föredrar personligen [GitKraken](https://www.gitkraken.com/) men vet att det finns andra klienter så som [GitHub Desktop](https://desktop.github.com/), [SourceTree](https://www.sourcetreeapp.com/) och fler! Om du av någon anledning inte skulle gilla dessa så sök bara på Git GUI på Google eller valfri search engine så kommer nog många alternativ upp.

## Terminologi
att göra: lägg till ytterliggare terminologi och förklara existerande

- Repository
- Clone
- Fork
- Stage
- Unstage
- Commit
- Push
- Pull
- Pull Request
- Branch
- Merge